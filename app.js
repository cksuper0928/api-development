const express = require('express');
const app = express();
/// var request = require('request-promise');
var request = require('request');
var db = require('./db');
const URL = 'mongodb://localhost:27017/api_integration';
const PORT = process.env.PORT || 3001;
const xml = require("xml-parse");
var parseString = require('xml2js').parseString;
const authkey = "fe2696be255c2b42f71c8f13ae58c1df";
const username = "korafzllz"
var map = require('async/map')

// Connect to Mongo on start
db.connect(URL, function(err) {
    if (err) {
        console.log('Unable to connect to Mongo.');
        process.exit(1)
    } else {
        app.listen(PORT, function() {
            console.log('Listening on port ' + PORT + '...');
        })
    }
});

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  next();
});

//API* ---tbl_areas--- *//
app.get('/areas', function (req, res, next) {
    var apiUrl = 'http://api.core.optasports.com/soccer/get_areas?authkey='+authkey+'&username='+username;
    var apiUrlAr = 'http://api.core.optasports.com/soccer/get_areas?lang=ar&authkey='+authkey+'&username='+username;
    var collection = db.get().collection('tbl_areas');

    request(apiUrl, function (error, response, body) {

        var areaData= xml.parse(body);

        var areaList = areaData[2].childNodes;
        
        request(apiUrlAr, function (error, response, body) 
        {
            var areaDataAr= xml.parse(body);
            var areaListAr = areaDataAr[2].childNodes;
        
            areaList.map((item, i) => {
                
                if (item.tagName == "area") {
                    var new_data = {
                        area_id: parseInt(item.attributes.area_id),
                        name_en: item.attributes.name,
                        name_ar: areaListAr[i].attributes.name,
                        clean_url_en: item.attributes.name.toLowerCase(),
                        clean_url_ar: areaListAr[i].attributes.name,
                        country_code: item.attributes.countrycode,
                        iso2: "",
                        parent: "",
                        sort_order: ""
                    }

                    collection.update(
                        {area_id : new_data.area_id},
                        {$set : new_data}, 
                        {upsert : true}
                    );
                }
            });
            res.send(areaListAr);
        })
    })
});

//API* ---tbl_competitions--- *//
app.get('/competitions', function (req, res, next) {
    var apiUrl = 'http://api.core.optasports.com/soccer/get_competitions?authorized=yes&authkey='+authkey+'&username='+username;
    var apiUrlAr = 'http://api.core.optasports.com/soccer/get_competitions?authorized=yes&lang=ar&authkey='+authkey+'&username='+username;
    var collection = db.get().collection('tbl_competitions');

    request(apiUrl, function (error, response, body) {

        var competitionData= xml.parse(body);

        var areaList = competitionData[2].childNodes;
        request(apiUrlAr, function (error, response, body) 
        {
            var competitionDataAr= xml.parse(body);
            var areaListAr = competitionDataAr[2].childNodes;            

            areaList.map((item, i) => {
                    
                if (item.tagName == "competition") {
                    var new_data = {
                        competition_id: parseInt(item.attributes.competition_id),
                        ow_competition_id: parseInt(item.attributes.ow_competition_id),
                        area_id: parseInt(item.attributes.area_id),
                        competition_name_en: item.attributes.name,
                        competition_name_ar: areaListAr[i].attributes.name,
                        competition_display_name_en: null,
                        competition_display_name_ar: null,
                        competition_clean_url_en: item.attributes.name.toLowerCase(),
                        competition_clean_url_ar: areaListAr[i].attributes.name,
                        teamtype: item.attributes.teamtype,
                        type: item.attributes.type,
                        format: item.attributes.format,
                        display_order: item.attributes.display_order,
                        current_season_id: null,
                        sort_order: null,
                        image_id: null
                    }

                    collection.update(
                        {competition_id : new_data.competition_id},
                        {$set : new_data}, 
                        {upsert : true}
                    );
                }
            });

            res.send(areaList);
        });
    })
});


//API* ---tbl_seasons--- *//
app.get('/seasons', function (req, res, next) {
    var apiUrl = 'http://api.core.optasports.com/soccer/get_seasons?authorized=yes&authkey='+authkey+'&username='+username;
    var apiUrlAr = 'http://api.core.optasports.com/soccer/get_seasons?authorized=yes&lang=ar&authkey='+authkey+'&username='+username;
    var collection = db.get().collection('tbl_seasons');

    request(apiUrl, function (error, response, body) {

        var areaData= xml.parse(body);

        var areaList = areaData[2].childNodes;

        request(apiUrlAr, function (error, response, body) 
        {
            var areaDataAr= xml.parse(body);

            var areaListAr = areaDataAr[2].childNodes;

            areaList.map((item, i) => {
                    
                if (item.tagName == "competition") {
                    var new_data = {
                        season_id: parseInt(item.childNodes[0].attributes.season_id),
                        competition_id: parseInt(item.attributes.competition_id),
                        season_name: item.childNodes[0].attributes.name,
                        season_clean_url: item.childNodes[0].attributes.name.toLowerCase(),
                        start_date: item.childNodes[0].attributes.start_date,
                        end_date: item.childNodes[0].attributes.end_date
                    }

                    collection.update(
                        {season_id : new_data.season_id},
                        {$set : new_data}, 
                        {upsert : true}
                    );
                }
            });

            res.send(areaList);
        });
    })
});

//API* ---tbl_rounds--- *//
app.get('/rounds', function (req, res, next) {
    var season_collection = db.get().collection('tbl_seasons');
    var collection = db.get().collection('tbl_rounds');

    season_collection.find().toArray(function(err, result) {
        if (err) throw err;

        arr_season = result;
    

        arr_season.map((season, i) => {
            var apiUrl = 'http://api.core.optasports.com/soccer/get_rounds?season_id='+season.season_id+'&authkey='+authkey+'&username='+username;
            var apiUrlAr = 'http://api.core.optasports.com/soccer/get_rounds?season_id='+season.season_id+'&lang=ar&authkey='+authkey+'&username='+username;
            
        
            request(apiUrl, function (error, response, body) {
        
                var areaData= xml.parse(body);
        
                var areaList = areaData[2].childNodes;
        
                    request(apiUrlAr, function (error, response, body) 
                    {
                        var areaDataAr= xml.parse(body);
            
                        var areaListAr = areaDataAr[2].childNodes;
            
                        areaList.map((item, ir) => {
                                
                            if (item.tagName == "competition") {

                                var arr_round = item.childNodes[0].childNodes;

                                arr_round.map((roudn_item, i) => {

                                    var new_data = {
                                        round_id: parseInt(roudn_item.attributes.round_id),
                                        season_id: parseInt(item.childNodes[0].attributes.season_id),
                                        round_name_en: roudn_item.attributes.name,
                                        round_name_ar: areaListAr[ir].childNodes[0].childNodes[i].attributes.name,
                                        display_name_en: null,
                                        display_name_ar: null,
                                        start_date: roudn_item.attributes.start_date,
                                        end_date: roudn_item.attributes.end_date,
                                        type: roudn_item.attributes.type,
                                        groups: roudn_item.attributes.groups
                                    }
                
                                    collection.update(
                                        {round_id : new_data.round_id},
                                        {$set : new_data}, 
                                        {upsert : true}
                                    );
                                });
                            }
                        });
                    });
            })
        });

    }); 
});


//API* ---tbl_groups--- *//
app.get('/groups', function (req, res, next) {
    var round_collection = db.get().collection('tbl_rounds');
    var collection = db.get().collection('tbl_groups');

    round_collection.find().toArray(function(err, result) {
        if (err) throw err;

        arr_round = result;
        

        if (arr_round) {
            arr_round.map((round, i) => {
                var apiUrl = 'http://api.core.optasports.com/soccer/get_groups?round_id='+round.round_id+'&authkey='+authkey+'&username='+username;
                var apiUrlAr = 'http://api.core.optasports.com/soccer/get_groups?round_id='+round.round_id+'&lang=ar&authkey='+authkey+'&username='+username;
                
            
                request(apiUrl, function (error, response, body) {
            
                    var groupsData= xml.parse(body);
                    if (groupsData[2]) {
                        var areaList = groupsData[2].childNodes;
                    
                        request(apiUrlAr, function (error, response, body) 
                        {
                            var groupsDataAr= xml.parse(body);

                            if(groupsDataAr[2]) {
                                var areaListAr = groupsDataAr[2].childNodes;
                
                                areaList.map((item, ir) => {
                                        
                                    if (item.tagName == "competition") {
                                        var arr_group = item.childNodes[0].childNodes[0].childNodes;
                                        
                                        arr_group.map((group_item, i) => {
                                            var new_data = {
                                                group_id: parseInt(group_item.attributes.group_id),
                                                round_id: parseInt(item.childNodes[0].childNodes[0].attributes.round_id),
                                                group_name_en: group_item.attributes.name,
                                                group_name_ar: areaListAr[ir].childNodes[0].childNodes[0].childNodes[i].attributes.name
                                            }
                        
                                            collection.update(
                                                {group_id : new_data.group_id},
                                                {$set : new_data}, 
                                                {upsert : true}
                                            );
                                        });
                                    }
                                });
                            }
                        });
                    }
                })
            });
        }
    }); 
});

//API* ---tbl_injuries--- *//
app.get('/injuries', function (req, res, next) {
    var competition_collection = db.get().collection('tbl_competitions');
    var collection = db.get().collection('tbl_injuries');

    competition_collection.find().toArray(function(err, result) {
        if (err) throw err;

        arr_competition = result;

        arr_competition.map((competition, i) => {
            var apiUrl = 'http://api.core.optasports.com/soccer/get_injuries?id='+competition.competition_id+'&type=competition&authkey='+authkey+'&username='+username;
            var apiUrlAr = 'http://api.core.optasports.com/soccer/get_injuries?id='+competition.competition_id+'&type=competition&lang=ar&authkey='+authkey+'&username='+username;
            
        
            request(apiUrl, function (error, response, body) {

                parseString(body, function (err, result) {
                    
                    if (!isEmpty(result)) {
                        arr_injuries = result.gsmrs.person

                        if (arr_injuries) {
                            arr_injuries.map((item, i) => {
                            
                                item.injury.map((injuery, j) => {
                                    var new_data = {
                                        competition_id : parseInt(competition.competition_id),
                                        person_id : parseInt(item.$.person_id),
                                        injury_type : injuery.$.type,
                                        start_date : injuery.$.start_date,
                                        end_date : injuery.$.end_date,
                                        expected_end_date : injuery.$.expected_end_date,
                                    }
    
                                    collection.update(
                                        {competition_id : new_data.competition_id, person_id: new_data.person_id},
                                        {$set : new_data}, 
                                        {upsert : true}
                                    );
                                });
                            });
                        }
                    }
                });
            })
        });

    }); 
});

//API* ---tbl_teams--- *//
app.get('/teams', function (req, res, next) {
    var competition_seasons = db.get().collection('tbl_seasons');
    var collection = db.get().collection('tbl_teams');
    
    competition_seasons.find().toArray(function(err, result) {
        if (err) throw err;

        result.map((season, s) => {
            

            var apiUrl_get_teams = "http://api.core.optasports.com/soccer/get_teams?id="+season.season_id+"&type=season&authkey="+authkey+"&username="+username;

            request(apiUrl_get_teams, function(error, response, body) {
                
                parseString(body, function (err, result) {

                    if (!isEmpty(result)) {
                        arr_team = result.gsmrs.team;

                        if (arr_team) {
                            arr_team.map((team, t) => {
                                var apiUrl = "http://api.core.optasports.com/soccer/get_teams?id="+team.$.team_id+"&type=team&detailed=yes&authkey="+authkey+"&username="+username;
                                
                                var apiUrlAr = "http://api.core.optasports.com/soccer/get_teams?lang=ar&id="+team.$.team_id+"&type=team&detailed=yes&authkey="+authkey+"&username="+username;
                                
                                request(apiUrl, function(error,response,team_body) {
                                    parseString(team_body, function (err, team_result) {
                                        if (team_result) {
                                            request(apiUrlAr, function(error,response,team_body_ar) {

                                                parseString(team_body_ar, function (err, team_result_ar) {

                                                    if (team_result_ar) {
                                                        
                                                        var new_data = {
                                                            team_id : parseInt(team_result.gsmrs.team[0].$.team_id),
                                                            area_id : parseInt(team_result.gsmrs.team[0].$.area_id),
                                                            team_name_en : team_result.gsmrs.team[0].$.club_name,
                                                            team_name_ar : team_result_ar.gsmrs.team[0].$.club_name,
                                                            official_name_en : team_result.gsmrs.team[0].$.official_name,
                                                            official_name_ar : team_result_ar.gsmrs.team[0].$.official_name,
                                                            short_name_en : team_result.gsmrs.team[0].$.short_name,
                                                            short_name_ar : team_result_ar.gsmrs.team[0].$.short_name,
                                                            tla_name : team_result.gsmrs.team[0].$.tla_name,
                                                            display_name_en : null,
                                                            display_name_ar : null,
                                                            team_clean_url_en : team_result.gsmrs.team[0].$.club_name,
                                                            team_clean_url_ar : team_result_ar.gsmrs.team[0].$.club_name,
                                                            type : team_result.gsmrs.team[0].$.type,
                                                            teamtype : team_result.gsmrs.team[0].$.teamtype,
                                                            address_en : team_result.gsmrs.team[0].$.address,
                                                            address_ar : team_result_ar.gsmrs.team[0].$.address,
                                                            address_zip : team_result.gsmrs.team[0].$.address_zip,
                                                            address_number : team_result.gsmrs.team[0].$.address_number,
                                                            address_extra : team_result.gsmrs.team[0].$.address_extra,
                                                            postal_address : team_result.gsmrs.team[0].$.postal_address,
                                                            postal_number : team_result.gsmrs.team[0].$.postal_number,
                                                            postal_extra : team_result.gsmrs.team[0].$.postal_extra,
                                                            postal_zip : team_result.gsmrs.team[0].$.postal_zip,
                                                            telephone : team_result.gsmrs.team[0].$.telephone,
                                                            fax : team_result.gsmrs.team[0].$.fax,
                                                            url : team_result.gsmrs.team[0].$.url,
                                                            email : team_result.gsmrs.team[0].$.email,
                                                            founded : team_result.gsmrs.team[0].$.founded,
                                                            club_colors : team_result.gsmrs.team[0].$.club_colors,
                                                            clothing : team_result.gsmrs.clothing,
                                                            sponsor : team_result.gsmrs.team[0].$.sponsor,
                                                            details : team_result.gsmrs.team[0].$.details,
                                                            sort_order : null,
                                                            image_id : null,
                                                            team_logo_fill_id : null,
                                                            verified : null,
                                                        };
    
                                                        collection.update(
                                                            {team_id : new_data.team_id},
                                                            {$set : new_data}, 
                                                            {upsert : true}
                                                        );
                                                    }
    
                                                });
                                                
                                            });
                                        }
                                    });

                                });
                            });
                        }                        
                    }
                    
                });
            });
        });
    });
});


//API* ---tbl_matches--- *//
app.get('/matches', function (req, res, next) {
    var competition_seasons = db.get().collection('tbl_seasons');
    var collection = db.get().collection('tbl_matches');
    
    competition_seasons.find().toArray(function(err, result) {
        if (err) throw err;

        result.map((season, s) => {
            

            var apiUrl_get_matches = "http://api.core.optasports.com/soccer/get_matches?id="+season.season_id+"&type=season&authkey="+authkey+"&username="+username;

            request(apiUrl_get_matches, function(error, response, body) {
                
                parseString(body, function (err, result) {
                    if (!isEmpty(result)) {
                        var arr_data = result.gsmrs.competition[0].season[0].round

                        if (arr_data) {
                            
                            map(arr_data, function(data) {                                
                                
                                if (data.match) {
                                    var arr_match = data.match

                                    if (arr_match) {

                                        arr_match.map((match, m) => {

                                            var match_data = {
                                                match_id : parseInt(match.$.match_id),
                                                area_id : parseInt(result.gsmrs.competition[0].$.area_id),
                                                competition_id : parseInt(result.gsmrs.competition[0].$.competition_id),
                                                season_id : parseInt(result.gsmrs.competition[0].season[0].$.season_id),
                                                round_id : parseInt(result.gsmrs.competition[0].season[0].round[0].$.round_id),
                                                group_id : 0,
                                                date_utc : match.$.date_utc,
                                                date_time_utc : '',
                                                team_a_id : parseInt(match.$.team_A_id),
                                                team_b_id : parseInt(match.$.team_B_id),
                                                team_a_name : match.$.team_A_name,
                                                team_b_name : match.$.team_B_name,
                                                match_clean_url : null,
                                                status : match.$.status,
                                                gameweek : match.$.gameweek,
                                                fs_A : match.$.fs_A,
                                                fs_B : match.$.fs_B,
                                                hts_A : match.$.hts_A,
                                                hts_B : match.$.hts_B,
                                                ets_A : match.$.ets_A,
                                                ets_B : match.$.ets_B,
                                                ps_A : match.$.ps_A,
                                                ps_B : match.$.ps_B,
                                                venue : null,
                                                referee : null,
                                                assistant_referee1 : null,
                                                assistant_referee2 : null,
                                                fourth_official : null,
                                                coach_team_a : null,
                                                coach_team_b : null,
                                                attendance : null,
                                                related_to : null,
                                                minute : null,
                                                minute_extra : null,
                                                match_period : null,
                                                important : 0,
                                                in_predict_win : 0,
                                                opta_match_id : null,
                                                opta_competition_id : null,
                                                opta_season_id : null,
                                                hoarding_image_id : null,
                                            };
                                            
                                            collection.update(
                                                {match_id : match_data.match_id},
                                                {$set : match_data}, 
                                                {upsert : true}
                                            );
                                        });                                        
                                    }
                                }

                            }, function(err, result){
                                console.log(err);
                            });
                        }
                    }
                });
            });
        });
    });
});

//API* ---tbl_countries--- *//
app.get('/countries', function (req, res, next) {
    var apiUrl = 'http://api.core.optasports.com/soccer/get_areas?authkey='+authkey+'&username='+username;
    var apiUrlAr = 'http://api.core.optasports.com/soccer/get_areas?lang=ar&authkey='+authkey+'&username='+username;
    var collection = db.get().collection('tbl_countries');

    request(apiUrl, function (error, response, body) {
        parseString(body, function (err, result) {
            if (result) {
                areaData = result;

                if (areaData) {
                    var areaList = areaData.gsmrs.area[0].area[0].area;

                    request(apiUrlAr, function (error, response, body) {
                            
                        parseString(body, function (err, result) {
                            
                            if (result) {
                                areaDataAr = result;

                                if (areaData) {
                                    var areaListAr = areaDataAr.gsmrs.area[0].area[0].area;

                                    areaList.map((item, i) => {
                                        var new_data = {
                                            country_name_en: item.$.name,
                                            country_name_ar: areaListAr[i].$.name,
                                            country_code: item.$.countrycode
                                        }
                            
                                        collection.update(
                                            {country_name_en : item.$.name},
                                            {$set : new_data}, 
                                            {upsert : true}
                                        );
                                    });
                                }
                            }
                        });
                    });
                }
            }
        });
        res.send("succeed!");
    })
});

//API* ---tbl_images--- *//
app.get('/images', function (req, res, next) {
    var teams_collection = db.get().collection('tbl_teams');
    var collection = db.get().collection('tbl_images');

    teams_collection.find().toArray(function(err, result) {
        result.map((item, i) => {
            var apiUrl = 'http://cache.images.core.optasports.com/soccer/teams/150x150/'+item.team_id+'.png'; 

            request(apiUrl, function (error, response, body) {
                res.send(body);
            });   
        });
    })
})

//API* ---tbl_matches_events--- *//
app.get('/matches_events', function (req, res, next) {
    var competition_seasons = db.get().collection('tbl_seasons');
    var collection = db.get().collection('tbl_matches_events');
    
    competition_seasons.find().toArray(function(err, result) {
        if (err) throw err;

        result.map((season, s) => {
            

            var apiUrl_get_matches = "http://api.core.optasports.com/soccer/get_matches?id="+season.season_id+"&type=season&authkey="+authkey+"&username="+username;

            request(apiUrl_get_matches, function(error, response, body) {
                
                parseString(body, function (err, result) {
                    if (!isEmpty(result)) {
                        var arr_data = result.gsmrs.competition[0].season[0].round

                        if (arr_data) {
                            arr_data.map((data, d) => {
                                if (data.match) {
                                    var arr_match = data.match
                                    
                                    arr_match.map((match, m) => {
                                        var apiUrl = "http://api.core.optasports.com/soccer/get_matches?type=match&id="+match.$.match_id+"&detailed=yes&authkey="+authkey+"&username="+username;
                                        var apiUrlAr = "http://api.core.optasports.com/soccer/get_matches?lang=ar&type=match&id="+match.$.match_id+"&detailed=yes&authkey="+authkey+"&username="+username;
                                        
                                        request(apiUrl, function(error,response,match_body) {
                                            parseString(match_body, function (err, match_result) {
                                                
                                                if (match_result) {
                                                    var match_id = match_result.gsmrs.competition[0].season[0].round[0].match[0].$.match_id;

                                                    if (match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0] != "") {

                                                        var match_data = {
                                                            event_id    : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.event_id),
                                                            match_id    : parseInt(match_id),
                                                            code        : match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.code,
                                                            person_id   : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.person_id),
                                                            team_id     : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.team_id),
                                                            minute      : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.minute),
                                                            related_to  : null, 
                                                            minute_extra: match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.minute_extra,
                                                            shirtnumber : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.shirtnumber),
                                                        }

                                                        collection.update(
                                                            {event_id : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0].event[0].$.event_id)},
                                                            {$set : match_data}, 
                                                            {upsert : true}
                                                        );
                                                    }

                                                    if (match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0] != "") {

                                                        var match_data = {
                                                            event_id    : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.event_id),
                                                            match_id    : parseInt(match_id),
                                                            code        : match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.code,
                                                            person_id   : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.person_id),
                                                            team_id     : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.team_id),
                                                            minute      : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.minute),
                                                            related_to  : null, 
                                                            minute_extra: match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.minute_extra,
                                                            shirtnumber : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.shirtnumber),
                                                        }

                                                        collection.update(
                                                            {event_id : parseInt(match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0].event[0].$.event_id)},
                                                            {$set : match_data}, 
                                                            {upsert : true}
                                                        );
                                                    }

                                                    if (match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups[0] == "") {

                                                        var match_data = {
                                                            event_id    : 0,
                                                            match_id    : match_id,
                                                            code        : 0,
                                                            person_id   : 0,
                                                            team_id     : 0,
                                                            minute      : null,
                                                            related_to  : null, 
                                                            minute_extra: null,
                                                            shirtnumber : null,
                                                        }

                                                        collection.update(
                                                            {match_id : match_id},
                                                            {$set : match_data}, 
                                                            {upsert : true}
                                                        );
                                                    }

                                                    if (match_result.gsmrs.competition[0].season[0].round[0].match[0].lineups_bench[0] == "") {

                                                        var match_data = {
                                                            event_id    : 0,
                                                            match_id    : match_id,
                                                            code        : 0,
                                                            person_id   : 0,
                                                            team_id     : 0,
                                                            minute      : null,
                                                            related_to  : null, 
                                                            minute_extra: null,
                                                            shirtnumber : null,
                                                        }

                                                        collection.update(
                                                            {match_id : match_id},
                                                            {$set : match_data}, 
                                                            {upsert : true}
                                                        );
                                                    }
                                                }
                                            });
                                        });
                                    });
                                }

                                if (data.group) {
                                    var arr_group = data.group;
                                    arr_group.map((group, g) => {

                                        if (group.match) {
                                            group.match.map((item, i) => {
                                                var apiUrl = "http://api.core.optasports.com/soccer/get_matches?type=match&id="+item.$.match_id+"&detailed=yes&authkey="+authkey+"&username="+username;
                                                var apiUrlAr = "http://api.core.optasports.com/soccer/get_matches?lang=ar&type=match&id="+item.$.match_id+"&detailed=yes&authkey="+authkey+"&username="+username;
                                                
                                                request(apiUrl, function(error,response,match_body) {
                                                    parseString(match_body, function (err, match_result) {

                                                        if (match_result) {

                                                            var match_id = match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].$.match_id;

                                                            if (match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0] != "") {

                                                                var match_data = {
                                                                    event_id    : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.event_id),
                                                                    match_id    : parseInt(match_id),
                                                                    code        : match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.code,
                                                                    person_id   : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.person_id),
                                                                    team_id     : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.team_id),
                                                                    minute      : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.minute),
                                                                    related_to  : null, 
                                                                    minute_extra: match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.minute_extra,
                                                                    shirtnumber : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.shirtnumber),
                                                                }

                                                                collection.update(
                                                                    {event_id : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0].event[0].$.event_id)},
                                                                    {$set : match_data}, 
                                                                    {upsert : true}
                                                                );
                                                            }

                                                            if (match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0] != "") {

                                                                var match_data = {
                                                                    event_id    : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.event_id),
                                                                    match_id    : parseInt(match_id),
                                                                    code        : match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.code,
                                                                    person_id   : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.person_id),
                                                                    team_id     : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.team_id),
                                                                    minute      : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.minute),
                                                                    related_to  : null, 
                                                                    minute_extra: match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.minute_extra,
                                                                    shirtnumber : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.shirtnumber),
                                                                }

                                                                collection.update(
                                                                    {event_id : parseInt(match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0].event[0].$.event_id)},
                                                                    {$set : match_data}, 
                                                                    {upsert : true}
                                                                );
                                                            }

                                                            if (match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups[0] == "") {

                                                                var match_data = {
                                                                    event_id    : 0,
                                                                    match_id    : match_id,
                                                                    code        : 0,
                                                                    person_id   : 0,
                                                                    team_id     : 0,
                                                                    minute      : null,
                                                                    related_to  : null, 
                                                                    minute_extra: null,
                                                                    shirtnumber : null,
                                                                }
        
                                                                collection.update(
                                                                    {match_id : match_id},
                                                                    {$set : match_data}, 
                                                                    {upsert : true}
                                                                );
                                                            }

                                                            if (match_result.gsmrs.competition[0].season[0].round[0].group[0].match[0].lineups_bench[0] == "") {

                                                                var match_data = {
                                                                    event_id    : 0,
                                                                    match_id    : match_id,
                                                                    code        : 0,
                                                                    person_id   : 0,
                                                                    team_id     : 0,
                                                                    minute      : null,
                                                                    related_to  : null, 
                                                                    minute_extra: null,
                                                                    shirtnumber : null,
                                                                }
        
                                                                collection.update(
                                                                    {match_id : match_id},
                                                                    {$set : match_data}, 
                                                                    {upsert : true}
                                                                );
                                                            }
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });
        });
        res.send("Succeed!")
    });
});

//API* ---tbl_matches_statistics--- *//
app.get('/matches_statistics', function (req, res, next) {
    var collection_matches = db.get().collection('tbl_matches');
    var collection = db.get().collection('tbl_matches_statistics');
    
    collection_matches.find().toArray(function(err, result) {
        result.map((item ,i) => {
            var apiUrl = "http://api.core.optasports.com/soccer/get_match_statistics?id="+item.match_id+"&authkey="+authkey+"&username="+username;
            
            
            request(apiUrl, function(error,response,body) {
                parseString(body, function (err, result) {
                    if (result) {
                        
                        var arr_statistics = result.gsmrs.competition[0].season[0].round[0].match[0].data;

                        arr_statistics.map((item, i) => {
                            if (item.$.type == "attacks") {
                                var attacks_a = item.$.value_team_A;
                                var attacks_b = item.$.value_team_B;
                            }
                            if (item.$.type == "corners") {
                                var corners_a = item.$.value_team_A;
                                var corners_b = item.$.value_team_B;
                            }
                            if (item.$.type == "dangerous_attacks") {
                                var dangerous_attacks_a = item.$.value_team_A;
                                var dangerous_attacks_b = item.$.value_team_B;
                            }
                            if (item.$.type == "fouls") {
                                var fouls_a = item.$.value_team_A;
                                var fouls_b = item.$.value_team_B;
                            }
                            if (item.$.type == "free_kicks") {
                                var free_kicks_a = item.$.value_team_A;
                                var free_kicks_b = item.$.value_team_B;
                            }
                            if (item.$.type == "goals") {
                                var goals_a = item.$.value_team_A;
                                var goals_b = item.$.value_team_B;
                            }
                            if (item.$.type == "goal_kicks") {
                                var goal_kicks_a = item.$.value_team_A;
                                var goal_kicks_b = item.$.value_team_B;
                            }
                            if (item.$.type == "offsides") {
                                var offsides_a = item.$.value_team_A;
                                var offsides_b = item.$.value_team_B;
                            }
                            if (item.$.type == "shots_blocked") {
                                var shots_blocked_a = item.$.value_team_A;
                                var shots_blocked_b = item.$.value_team_B;
                            }
                            if (item.$.type == "shots_wide") {
                                var shots_off_target_a = item.$.value_team_A;
                                var shots_off_target_b = item.$.value_team_B;
                            }
                            if (item.$.type == "shots_target") {
                                var shots_on_target_a = item.$.value_team_A;
                                var shots_on_target_b = item.$.value_team_B;
                            }
                            if (item.$.type == "shots_woodwork") {
                                var shots_woodwork_a = item.$.value_team_A;
                                var shots_woodwork_b = item.$.value_team_B;
                            }
                            if (item.$.type == "throw_ins") {
                                var throw_ins_a = item.$.value_team_A;
                                var throw_ins_b = item.$.value_team_B;
                            }
                            if (item.$.type == "substitutions") {
                                var substitutions_a = item.$.value_team_A;
                                var substitutions_b = item.$.value_team_B;
                            }
                            if (item.$.type == "yellow cards") {
                                var yellow_cards_a = item.$.value_team_A;
                                var yellow_cards_b = item.$.value_team_B;
                            }

                            statistics_data = {
                                match_id : result.gsmrs.competition[0].season[0].round[0].match[0].$.match_id,
                                attacks_a : attacks_a,
                                attacks_b : attacks_b,
                                corners_a : corners_a,
                                corners_b : corners_b,
                                dangerous_attacks_a : dangerous_attacks_a,
                                dangerous_attacks_b : dangerous_attacks_b,
                                fouls_a : fouls_a,
                                fouls_b : fouls_b,
                                free_kicks_a : free_kicks_a,
                                free_kicks_b : free_kicks_b,
                                goals_a : goals_a,
                                goals_b : goals_b,
                                goal_kicks_a : goal_kicks_a,
                                goal_kicks_b : goal_kicks_b,
                                offsides_a : offsides_a,
                                offsides_b : offsides_b,
                                shots_blocked_a : shots_blocked_a,
                                shots_blocked_b : shots_blocked_b,
                                shots_off_target_a : shots_off_target_a,
                                shots_off_target_b : shots_off_target_b,
                                shots_on_target_a : shots_on_target_a,
                                shots_on_target_b : shots_on_target_b,
                                shots_woodwork_a : shots_woodwork_a,
                                shots_woodwork_b : shots_woodwork_b,
                                penalties_a : null,
                                penalties_b : null,
                                throw_ins_a : throw_ins_a,
                                throw_ins_b : throw_ins_b,
                                yellow_cards_a : yellow_cards_a,
                                yellow_cards_b : yellow_cards_b,
                                red_cards_a : null,
                                red_cards_b : null,
                                yellow_red_cards_a : null,
                                yellow_red_cards_b : null,
                                possession_a : null,
                                possession_b : null,
                                substitutions_a : substitutions_a,
                                substitutions_b : substitutions_b,
                                posession_a : null,
                                posession_b : null,
                            };

                            collection.update(
                                {match_id : result.gsmrs.competition[0].season[0].round[0].match[0].$.match_id},
                                {$set : statistics_data},
                                {upsert : true}
                            );
                        });
                    }
                });
            });
        });
        res.send("Succeed!");
    });
});

//API* ---tbl_player_statistics--- *//
app.get('/player_statistics', function (req, res, next) {
    var collection_matches = db.get().collection('tbl_seasons');
    var collection = db.get().collection('tbl_player_statistics');
    
    collection_matches.find().toArray(function(err, result) { 
        result.map((item, i) => {
            apiUrl = "http://api.core.optasports.com/soccer/get_player_statistics?id="+item.season_id+"&type=season&authkey="+authkey+"&username="+username;

            request(apiUrl, function(error, result, body) {
                parseString(body, function (err, result) {
                    arr_competition = result.gsmrs.competition[0].season[0];
                    var competition_id = result.gsmrs.competition[0].$.competition_id;
                    var season_id = result.gsmrs.competition[0].season[0].$.season_id;

                    var arr_yellow_cards = arr_competition.yellow_cards[0].person;

                    if (arr_yellow_cards) {
                        arr_yellow_cards.map((item, i) => {
                            var yellow_cards_num = [];
                            
                            for(j = 0; j < arr_yellow_cards.length; j++){
                                if (item.$.person_id == arr_competition.yellow_cards[0].person[j].$.person_id){
                                    yellow_cards_num.push(item.$.person_id);
                                }
                            }
    
                            statistics_data = {
                                competition_id : competition_id,
                                season_id : season_id,
                                person_id : item.$.person_id,
                                team_id : item.team_id,
                                yellow_cards : yellow_cards_num.length,
                                red_cards : 0,
                                goals : 0,
                                assists : 0,
                            };

                            collection.update(
                                {person_id : item.$.person_id},
                                {$set : statistics_data},
                                {upsert : true}
                            );
                        });
                    }

                    var arr_red_cards = arr_competition.red_cards[0].person;

                    if (arr_red_cards) {
                        arr_red_cards.map((item, i) => {
                            var red_cards_num = [];
                            
                            for(j = 0; j < arr_red_cards.length; j++){
                                if (item.$.person_id == arr_competition.red_cards[0].person[j].$.person_id){
                                    red_cards_num.push(item.$.person_id);
                                }
                            }
    
                            statistics_data = {
                                competition_id : competition_id,
                                season_id : season_id,
                                person_id : item.$.person_id,
                                team_id : item.team_id,
                                yellow_cards : 0,
                                red_cards : red_cards_num.length,
                                goals : 0,
                                assists : 0,
                            };

                            collection.update(
                                {person_id : item.$.person_id},
                                {$set : statistics_data},
                                {upsert : true}
                            );
                        });
                    }

                    var arr_goals = arr_competition.goals[0].person;

                    if (arr_goals) {
                        arr_goals.map((item, i) => {
                            var goals_num = [];
                            
                            for(j = 0; j < arr_goals.length; j++){
                                if (item.$.person_id == arr_competition.goals[0].person[j].$.person_id){
                                    goals_num.push(item.$.person_id);
                                }
                            }
    
                            statistics_data = {
                                competition_id : competition_id,
                                season_id : season_id,
                                person_id : item.$.person_id,
                                team_id : item.team_id,
                                yellow_cards : 0,
                                red_cards : 0,
                                goals : goals_num.length,
                                assists : 0,
                            };

                            collection.update(
                                {person_id : item.$.person_id},
                                {$set : statistics_data},
                                {upsert : true}
                            );
                        });
                    }

                    var arr_assists = arr_competition.assists[0].person;

                    if (arr_assists) {
                        arr_assists.map((item, i) => {
                            var assists_num = [];
                            
                            for(j = 0; j < arr_assists.length; j++){
                                if (item.$.person_id == arr_competition.assists[0].person[j].$.person_id){
                                    assists_num.push(item.$.person_id);
                                }
                            }
    
                            statistics_data = {
                                competition_id : competition_id,
                                season_id : season_id,
                                person_id : item.$.person_id,
                                team_id : item.team_id,
                                yellow_cards : 0,
                                red_cards : 0,
                                goals : 0,
                                assists : assists_num.length,
                            };

                            collection.update(
                                {person_id : item.$.person_id},
                                {$set : statistics_data},
                                {upsert : true}
                            );
                        });
                    }

                });
            })
        });
        res.send("Succeed!");
    });
});

//API* ---tbl_rankings--- *//
app.get('/rankings', function (req, res, next) {
    var apiUrl = "http://api.core.optasports.com/soccer/get_rankings?type=fifa&authkey="+authkey+"&username="+username;
    var colletion = db.get().collection('tbl_rankings');

    request(apiUrl, function(error, result, body){
        parseString(body, function(err, result) {
            arr_rankings = result.gsmrs.rankings[0].ranking;
            
            arr_rankings.map((item, i) => {
                ranking_data = {
                    ranking_id  : item.$.ranking_id,
                    area_id     : item.$.area_id,
                    team_id     : item.$.$team_id,
                    type        : item.$.type,
                    points      : item.$.points,
                    position    : item.$.position,
                    year        : result.gsmrs.method[0].parameter[3].$.value,
                    month       : result.gsmrs.method[0].parameter[1].$.value,
                };

                colletion.update(
                    {ranking_id : item.$.ranking_id},
                    {$set       : ranking_data},
                    {upsert     : true}
                );
            });
            res.send("Succeed");
        })
    });
});


//Function *-- Check if object is empty --* //
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

app.get('/parse', function (req, res, next) {
    var apiUrl = 'http://api.core.optasports.com/soccer/get_matches?type=match&id=2412182&detailed=yes&authkey=fe2696be255c2b42f71c8f13ae58c1df&username=korafzllz';
    request(apiUrl, function (error, response, body) {
        parseString(body, function (err, result) {
            res.send(result.gsmrs.team);
        });
        
    });
})


//Skipped Tables//
// tbl_caricature
// tbl_channels
// tbl_coverage_tmp
// tbl_email_log
// tbl_games
// tbl_game_competitions_score
// tbl_game_prediction
// tbl_images -- not completed
// tbl_image_thumbs
// tbl_languages
// tbl_languages_labels
// tbl_languages_labels_translations
// tbl_match_commentators
// tbl_memberships
// tbl_news
// tbl_new_assign
// tbl_opta_queue
// tbl_pages
// tbl_persons
// tbl_persons_join_team
//

